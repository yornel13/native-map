import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'app',
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                loadChildren: () => import('./pages/home/tabs/tabs-page.module').then(m => m.TabsModule)
            },
            {
                path: '**',
                redirectTo: 'home',
                pathMatch: 'full'
            },
        ]
    },
    {
        path: '**',
        redirectTo: 'tabs',
        pathMatch: 'full'
    },
    {
        path: '',
        redirectTo: 'app/home/map',
        pathMatch: 'full'
    },
  {
    path: 'profile',
    loadChildren: () => import('./pages/home/tabs/profile/profile.module').then(m => m.ProfilePageModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('./pages/home/tabs/orders/orders.module').then(m => m.OrdersPageModule)
  },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
