import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'modal-page',
    templateUrl: 'modal.page.html'
})
export class ModalPage {

    constructor(public modalController: ModalController) {}

    dismissModal() {
        this.modalController.dismiss({
            dismissed: true
        });
    }
}
