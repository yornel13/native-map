import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { MapPage } from './map.page';
import { GoogleMaps } from '@ionic-native/google-maps';

import { HomePageRoutingModule } from './map-routing.module';
import { ModalPage } from '../modal/modal.page';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        IonBottomDrawerModule
    ],
    declarations: [MapPage, ModalPage],
    providers: [
        GoogleMaps
    ]
})
export class MapPageModule {
}
