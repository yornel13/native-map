import { Environment, GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsEvent, Marker } from '@ionic-native/google-maps';
import { AfterViewInit, Component } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { DrawerState } from 'ion-bottom-drawer';

const removeDefaultMarkers = [
    {
        featureType: 'poi',
        elementType: 'labels',
        stylers: [
            {visibility: 'off'}
        ]
    }
];

@Component({
    selector: 'app-home',
    templateUrl: 'map.page.html',
    styleUrls: ['map.page.scss'],
})
export class MapPage implements AfterViewInit {

    bottomDrawer = {
        shouldBounce: false,
        distanceTop: /*62*/0,
        dockedHeight: 494,
        minimumHeight: 0,
        drawerState: DrawerState.Bottom,
    };

    map: GoogleMap;
    fabAttached = true;

    constructor(private platform: Platform, public modalController: ModalController) { }

    loadMap() {

        // This code is necessary for browser
        Environment.setEnv({
            API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyD3t5VAdEBMdICcY9FyVcgBHlkeu72OI4s',
            API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyD3t5VAdEBMdICcY9FyVcgBHlkeu72OI4s'
        });

        const mapOptions: GoogleMapOptions = {
            camera: {
                target: {
                    lat: 43.0741904,
                    lng: -89.3809802
                },
                zoom: 18,
                tilt: 30
            },
            styles: removeDefaultMarkers
        };

        this.map = GoogleMaps.create('map_canvas', mapOptions);

        const marker: Marker = this.map.addMarkerSync({
            title: 'Ionic',
            icon: 'blue',
            animation: 'DROP',
            position: {
                lat: 43.0741904,
                lng: -89.3809802
            }
        });
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
            alert('clicked');
        });
    }

    ngAfterViewInit() {
        if (this.platform.is('cordova')) {
            this.loadMap();
        }
    }

    attachedPosition() {

    }

    showList() {
        this.bottomDrawer.drawerState = DrawerState.Top;
    }
}
